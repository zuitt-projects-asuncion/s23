/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session23)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

//Delete

	>> Delete all inactive courses


//Add all of your query/commands here in activity.js

*/


// Hi ma'am Tin.  Just want to inform you that I was able to connect my machine to the prescribed external IP yesterday.  Thank you. :)
db.collection.insertOne({
		"avengers" : "Avengers"
})

db.users.insertMany([
			{
				"firstName" : "Tony",
				"lastName" : "Stark",
				"email" : "tStark@mail.com",
				"password" : "IamIronMan",
				"isAdmin" : false
			},
			{
				"firstName" : "Steve",
				"lastName" : "Rogers",
				"email" : "cap@mail.com",
				"password" : "Icandothisallday",
				"isAdmin" : false
			},
			{
				"firstName" : "Thor",
				"lastName" : "Odinson",
				"email" : "thor@mail.com",
				"password" : "strongestAvenger",
				"isAdmin" : false
			},
			{
				"firstName" : "Natash",
				"lastName" : "Romanov",
				"email" : "nat@mail.com",
				"password" : "blackwidow",
				"isAdmin" : false
			},
			{
				"firstName" : "Bruce",
				"lastName" : "Banner",
				"email" : "gamma1@mail.com",
				"password" : "HulkSmash",
				"isAdmin" : false
			}
		])

db.collection.insertMany([
   {
    "course": "Anatomy101",
    "price" : 5000,
    "isActive" : false,
	},
	{
    "course": "Programming101",
    "price" : 5000,
    "isActive" : false,
     },
     {
    "course": "Physical Education 1",
    "price" : 1000,
    "isActive" : false,
     }
])

	db.collection.findMany({"isAdmin" : true})

	db.users.updateOne({}, {$set: {"isAdmin" : true}})

	db.collection.updateOne({"course" : "Programming101"}, {$set: {"isActive" : true}})

	db.collection.deleteMany({"isActive" : false})

